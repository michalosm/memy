package pl.akademiakodu.kwejk.dao;

import pl.akademiakodu.kwejk.model.Gif;

import java.util.List;

/**
 * Created by michalos on 04.09.2017.
 */

public interface GifDao {
    List<Gif> findAll();
}
